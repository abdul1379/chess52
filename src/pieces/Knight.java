package pieces;

import java.util.HashSet;

import chess.Board;
import chess.Square;
import pieceInterfaces.PieceMove;

/**
 * A Knight implementation of the abstract Piece class.
 * @author Abdulrahman Abdulrahman and Muhmmad Choudhary
 *
 */
public class Knight extends Piece {

	/**
	 * Constructor for the piece. Calls the super constructor.
	 * @param color - the piece's color
	 * @param coord - the String coordinate of the piece
	 * @param b - a reference to the board
	 */
	public Knight(Color color, String coord, Board b) {
		super(color, coord, b);
	}

	@Override
	protected void fillMovesAndAttacks() {
		int col = position.col;
		int row = position.row;
		
		PieceMove check = (Piece p, Square s, HashSet<String> path) -> {
			Piece temp = p.board.pieceAt(s);
			if(temp == null) {
				p.availableAttacks.add(s.toString());
				p.availableMoves.add(s.toString());
			}else if(temp.color == p.color) {
				p.availableAttacks.add(s.toString());
			}else {
				if(temp instanceof King) {
					King k = (King) temp;
					k.checkAttackPath = new HashSet<String>();
				}
				p.availableAttacks.add(s.toString());
				p.availableMoves.add(s.toString());
			}
			return PieceMove.PATH_CLEAR;
		};
		
		// Moving down 2 and left 1
		try {
			Square s = new Square(col - 1, row + 2);
			check.moveCheck(this, s, null);
		}catch(IllegalArgumentException e) {}
		
		// Moving down 2 right 1
		try {
			Square s = new Square(col + 1, row + 2);
			check.moveCheck(this, s, null);
		}catch(IllegalArgumentException e) {}
		
		// Moving up 2 right 1
		try {
			Square s = new Square(col + 1, row - 2);
			check.moveCheck(this, s, null);
		}catch(IllegalArgumentException e) {}
		
		// Moving up 2 left 1
		try {
			Square s = new Square(col - 1, row - 2);
			check.moveCheck(this, s, null);
		}catch(IllegalArgumentException e) {}
		
		// Moving left 2 down 1
		try {
			Square s = new Square(col - 2, row + 1);
			check.moveCheck(this, s, null);
		}catch(IllegalArgumentException e) {}
		
		// Moving left 2 up 1
		try {
			Square s = new Square(col - 2, row - 1);
			check.moveCheck(this, s, null);
		}catch(IllegalArgumentException e) {}
		
		// Moving right 2 down 1
		try {
			Square s = new Square(col + 2, row + 1);
			check.moveCheck(this, s, null);
		}catch(IllegalArgumentException e) {}
		
		// Moving right 2 up 1
		try {
			Square s = new Square(col + 2, row - 1);
			check.moveCheck(this, s, null);
		}catch(IllegalArgumentException e) {}
	}

	@Override
	public String toString() {
		if(color == WHITE) {
			return "wN";
		}else {
			return "bN";
		}
	}
}
