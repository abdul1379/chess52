package pieces;

import java.util.HashSet;

import chess.Board;
import chess.Square;

/**
 * Abstract class that contains all common attributes of chess pieces.
 * @author Abdulrahman Abdulrahman and Muhmmad Choudhary
 *
 */
public abstract class Piece {
	
	/**
	 * Enum for color of Players and Pieces.
	 * @author Abdulrahman Abdulrahman and Muhmmad Choudhary
	 *
	 */
	public static enum Color{
		WHITE, BLACK
	}
	
	/**
	 * White color reference in Piece class so all subclasses don't need to
	 * import and use {@link Color#WHITE}
	 */
	public static final Color WHITE = Color.WHITE;
	/**
	 * Black color reference in Piece class so all subclasses don't need to
	 * import and use {@link Color#BLACK}
	 */
	public static final Color BLACK = Color.BLACK;
	
	/**
	 * This piece's color.
	 */
	public Color color;
	/**
	 * This piece's position on the board.
	 */
	public Square position;
	/**
	 * All of this piece's available moves.
	 */
	public HashSet<String> availableMoves = new HashSet<String>();
	/**
	 * All of the squares that this piece can attack.
	 */
	public HashSet<String> availableAttacks = new HashSet<String>();
	/**
	 * A reference to the board.
	 */
	public Board board;
	
	/**
	 * Constructor for the piece.
	 * @param color - the piece's color
	 * @param coord - the String coordinate of the piece
	 * @param b - a reference to the board
	 * @throws IllegalArgumentException if any of the arguments are null, or the
	 * piece's coordinate is out of the bounds of the board.
	 */
	protected Piece(Color color, String coord, Board b) throws IllegalArgumentException {
		if(color == null || coord == null || b == null) {
			throw new IllegalArgumentException("Received a null argument for Piece");
		}
		this.position = Square.getSquare(coord);
		this.color = color;
		this.board = b;
	}
	
	/**
	 * Checks if the piece can move to the specified coordinate.
	 * @param s - the coordinate to check
	 * @return True if the piece can move to the coordinate and false otherwise.
	 */
	public boolean canMoveTo(String s) {
		update();
		return availableMoves.contains(s);
	}
	
	/**
	 * Moves the piece to the coordinate specified by s.
	 * @param s - the coordinate to move to
	 * @throws IllegalArgumentException if this piece cannot move to the
	 * coordinate.
	 */
	public void moveTo(String s) throws IllegalArgumentException {
		if(!canMoveTo(s)) {
			throw new IllegalArgumentException("Can't move to that coordinate");
		}
		Square prev = position;
		this.position = Square.getSquare(s);
		board.enPassantPawn = null;
		board.pawnToPromote = null;
		board.tempPiece = null;
		
		board.removePieceAt(position);
		board.board[prev.row][prev.col] = null;
		board.board[position.row][position.col] = this;
	}
	
	/**
	 * Clear's this piece's available moves and available attacks. Gets called
	 * before the moves and attacks are updated.
	 */
	private void clear() {
		availableMoves.clear();
		availableAttacks.clear();
	}
	
	/**
	 * Fills this piece's moves and attacks according to the guidelines of the 
	 * specific piece.
	 */
	protected abstract void fillMovesAndAttacks();
	
	/**
	 * Update's this piece's avaiable moves and attacks.
	 */
	public void update() {
		clear();
		fillMovesAndAttacks();
	}
	
	/**
	 * Returns the string representation of this specific piece.
	 */
	public abstract String toString();
}
