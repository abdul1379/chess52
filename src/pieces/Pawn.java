package pieces;

import java.util.HashSet;

import chess.Board;
import chess.Square;

/**
 * A Pawn implementation of the abstract SpecialFirstMovePiece class.
 * @author Abdulrahman Abdulrahman and Muhmmad Choudhary
 *
 */
public class Pawn extends SpecialFirstMovePiece {

	/**
	 * Constructor for the piece. Calls the super constructor.
	 * @param color - the piece's color
	 * @param coord - the String coordinate of the piece
	 * @param b - a reference to the board
	 */
	public Pawn(Color color, String coord, Board b) {
		super(color, coord, b);
	}
	
	/**
	 * Checks if the pawn can move to the specified coordinate. If the pawn moves
	 * two spaces on the first move {@link Board#enPassantPawn} gets set to this
	 * pawn. If the pawn reaches the end of the board {@link Board#pawnToPromote}
	 * gets set to this pawn.
	 */
	@Override
	public void moveTo(String s) throws IllegalArgumentException {
		if(!canMoveTo(s)) {
			throw new IllegalArgumentException();
		}
		Square prev = position;
		this.position = Square.getSquare(s);
		board.pawnToPromote = null;
		board.tempPiece = null;
		
		board.removePieceAt(position);
		board.board[prev.row][prev.col] = null;
		board.board[position.row][position.col] = this;
		
		if(color == WHITE) {
			if(position.row == 0) {
				// Deals with promotion
				board.pawnToPromote = this;
			}else if(numberOfMoves == 0 && position.row == 4) {
				// Deals with moving two spaces on first move
				board.enPassantPawn = this;
				numberOfMoves++;
				return;
			}
			if(position.col != prev.col) {
				// Deals with en passant attack
				Square enPass = new Square(position.col, 3);
				if(position.row == 2
						&& board.enPassantPawn == board.pieceAt(enPass)) {
					board.removePieceAt(enPass);
				}
			}
		}else {
			if(position.row == 7) {
				// Deals with promotion
				board.pawnToPromote = this;
			}else if(numberOfMoves == 0 && position.row == 3) {
				// Deals with moving two spaces on first move
				board.enPassantPawn = this;
				numberOfMoves++;
				return;
			}
			if(position.col != prev.col) {
				// Deals with en passant attack
				Square enPass = new Square(position.col, 4);
				if(position.row == 5
						&& board.enPassantPawn == board.pieceAt(enPass)) {
					board.removePieceAt(enPass);
				}
			}
		}
		numberOfMoves++;
		board.enPassantPawn = null;
	}

	@Override
	protected void fillMovesAndAttacks() {
		if(color == WHITE) {
			whiteFill();
		}else {
			blackFill();
		}
	}
	
	/**
	 * Fills the available moves and attacks for a black pawn.
	 */
	private void blackFill() {
		
		// Deals with moving forward
		try {
			Square forwardOne = new Square(position.col, position.row + 1);
			if(board.pieceAt(forwardOne) == null) {
				this.availableMoves.add(forwardOne.toString());
				
				// If at the starting position, allow moving two spaces
				if(position.row == 1) {
					Square forwardTwo = new Square(position.col, position.row + 2);
					if(board.pieceAt(forwardTwo) == null) {
						this.availableMoves.add(forwardTwo.toString());
					}
				}
			}
		}catch(IllegalArgumentException e) {}
		
		// Deals with attacking (forward left)
		try {
			Square s = new Square(position.col - 1, position.row + 1);
			Piece p = board.pieceAt(s);
			this.availableAttacks.add(s.toString());
			if(p != null && p.color != this.color) {
				if(p instanceof King) {
					King k = (King) p;
					k.checkAttackPath = new HashSet<String>();
				}
				this.availableMoves.add(s.toString());
			}
			// Deals with En Passant move
			Square adjacentLeft = new Square(position.col - 1, position.row);
			Piece enPass = board.pieceAt(adjacentLeft);
			if(position.row == 4
					&& enPass instanceof Pawn
					&& enPass == board.enPassantPawn) {
				this.availableMoves.add(s.toString());
			}
		}catch(IllegalArgumentException e) {}
		
		//Deals with attacking (forward right)
		try {
			Square s = new Square(position.col + 1, position.row + 1);
			Piece p = board.pieceAt(s);
			this.availableAttacks.add(s.toString());
			if(p != null && p.color != this.color) {
				if(p instanceof King) {
					King k = (King) p;
					k.checkAttackPath = new HashSet<String>();
				}
				this.availableMoves.add(s.toString());
			}
			// Deals with En Passant move
			Square adjacentRight = new Square(position.col + 1, position.row);
			Piece enPass = board.pieceAt(adjacentRight);
			if(position.row == 4
					&& enPass instanceof Pawn
					&& enPass == board.enPassantPawn) {
				this.availableMoves.add(s.toString());
			}
		}catch(IllegalArgumentException e) {}
	}
	
	/**
	 * Fills the available moves and attacks for a white pawn.
	 */
	private void whiteFill() {
		
		// Deals with moving forward
		try {
			Square forwardOne = new Square(position.col, position.row - 1);
			if(board.pieceAt(forwardOne) == null) {
				this.availableMoves.add(forwardOne.toString());
				
				// If at the starting position, allow moving two spaces
				if(position.row == 6) {
					Square forwardTwo = new Square(position.col, position.row - 2);
					if(board.pieceAt(forwardTwo) == null) {
						this.availableMoves.add(forwardTwo.toString());
					}
				}
			}
		}catch(IllegalArgumentException e) {}
		
		// Deals with attacking (forward left)
		try {
			Square s = new Square(position.col - 1, position.row - 1);
			Piece p = board.pieceAt(s);
			this.availableAttacks.add(s.toString());
			if(p != null && p.color != this.color) {
				if(p instanceof King) {
					King k = (King) p;
					k.checkAttackPath = new HashSet<String>();
				}
				this.availableMoves.add(s.toString());
			}
			// Deals with En Passant move
			Square adjacentLeft = new Square(position.col - 1, position.row);
			Piece enPass = board.pieceAt(adjacentLeft);
			if(position.row == 3
					&& enPass instanceof Pawn
					&& enPass == board.enPassantPawn) {
				this.availableMoves.add(s.toString());
			}
		}catch(IllegalArgumentException e) {}
		
		//Deals with attacking (forward right)
		try {
			Square s = new Square(position.col + 1, position.row - 1);
			Piece p = board.pieceAt(s);
			this.availableAttacks.add(s.toString());
			if(p != null && p.color != this.color) {
				if(p instanceof King) {
					King k = (King) p;
					k.checkAttackPath = new HashSet<String>();
				}
				this.availableMoves.add(s.toString());
			}
			// Deals with En Passant move
			Square adjacentRight = new Square(position.col + 1, position.row);
			Piece enPass = board.pieceAt(adjacentRight);
			if(position.row == 3
					&& enPass instanceof Pawn
					&& enPass == board.enPassantPawn) {
				this.availableMoves.add(s.toString());
			}
		}catch(IllegalArgumentException e) {}
	}

	@Override
	public String toString() {
		if(color == WHITE) {
			return "wp";
		}else {
			return "bp";
		}
	}

}
