package pieces;

import java.util.HashSet;
import java.util.function.BiConsumer;

import chess.Board;
import chess.Square;
import pieceInterfaces.PieceMove;


/**
 * A King implementation of the abstract SpecialFirstMovePiece class.
 * @author Abdulrahman Abdulrahman and Muhmmad Choudhary
 *
 */
public class King extends SpecialFirstMovePiece {

	/**
	 * Gets set when the King is under attack. Used to see if any piece can
	 * block the attack.
	 */
	public HashSet<String> checkAttackPath;
	
	/**
	 * Constructor for the piece. Calls the super constructor.
	 * @param color - the piece's color
	 * @param coord - the String coordinate of the piece
	 * @param b - a reference to the board
	 */
	public King(Color color, String coord, Board b) {
		super(color, coord, b);
	}
	
	/**
	 * Calls {@link Piece#moveTo(String)} and increments
	 * {@link SpecialFirstMovePiece#numberOfMoves}. If the King is castling
	 * the appropriate rook also gets moved to the proper position.
	 */
	@Override
	public void moveTo(String s) throws IllegalArgumentException {
		super.moveTo(s);
		
		// Deals with castling
		if(numberOfMoves == 0) {
			if(position.equals(new Square(6, startSquare.row))) {
				Rook right = (Rook) board.pieceAt(7, position.row);
				board.board[position.row][7] = null;
				right.position = new Square(5, position.row);
				right.numberOfMoves++;
				board.placePiece(right);
			}else if(position.equals(new Square(2, startSquare.row))) {
				Rook left = (Rook) board.pieceAt(0, position.row);
				board.board[position.row][0] = null;
				left.position = new Square(3, position.row);
				left.numberOfMoves++;
				board.placePiece(left);
			}
		}
		
		numberOfMoves++;
	}
	
	@Override
	protected void fillMovesAndAttacks() {
		
		HashSet<String> oppositeColorAttacks = board.getOppositeColorAttacks(color);
		PieceMove check = (Piece p, Square s, HashSet<String> attacks) -> {
			Piece temp = p.board.pieceAt(s);
			if(!attacks.contains(s.toString())) {
				if(temp == null) {
					p.availableAttacks.add(s.toString());
					p.availableMoves.add(s.toString());
				}else if(temp.color == p.color) {
					p.availableAttacks.add(s.toString());
				}else {
					p.availableAttacks.add(s.toString());
					p.availableMoves.add(s.toString());
				}
			}
			return PieceMove.PATH_CLEAR;
		};
		
		BiConsumer<Integer, Integer> checkSquare = (col, row) -> {
			try {
				Square s = new Square(col, row);
				check.moveCheck(this, s, oppositeColorAttacks);
			}catch(IllegalArgumentException e) {}
		};
		
		// Dealing with castling
		checkForCastling(oppositeColorAttacks);
		
		// Upper left move
		checkSquare.accept(position.col - 1, position.row - 1);
		
		// Directly above move
		checkSquare.accept(position.col, position.row - 1);
		
		// Upper right move
		checkSquare.accept(position.col + 1, position.row - 1);
		
		// Left move
		checkSquare.accept(position.col - 1, position.row);
		
		// Right move
		checkSquare.accept(position.col + 1, position.row);
		
		// Bottom left move
		checkSquare.accept(position.col - 1, position.row + 1);
		
		// Directly below move
		checkSquare.accept(position.col, position.row + 1);
		
		// Bottom right move
		checkSquare.accept(position.col + 1, position.row + 1);
	}
	
	/**
	 * Checks if the king can castle
	 * @param oppositeColorAttacks - the opponents available attacks. Used to
	 * ensure the King is able to castle and won't pass a square that is under
	 * attack.
	 */
	private void checkForCastling(HashSet<String> oppositeColorAttacks) {
		
		if(numberOfMoves == 0 && !oppositeColorAttacks.contains(position.toString())) {
			// Dealing with right rook
			Square fCol = new Square(position.col + 1, position.row);
			Square gCol = new Square(position.col + 2, position.row);
			if(board.pieceAt(fCol) == null
					&& board.pieceAt(gCol) == null
					&& !oppositeColorAttacks.contains(fCol.toString())
					&& !oppositeColorAttacks.contains(gCol.toString())) {
				Piece p = board.pieceAt(position.col + 3, position.row);
				if(p instanceof Rook && p.color == this.color) {
					Rook r = (Rook) p;
					if(r.numberOfMoves == 0) {
						this.availableMoves.add(gCol.toString());
					}
				}
			}
			
			// Dealing with left rook
			Square dCol = new Square(position.col - 1, position.row);
			Square cCol = new Square(position.col - 2, position.row);
			Square bCol = new Square(position.col - 3, position.row);
			if(board.pieceAt(dCol) == null
					&& board.pieceAt(cCol) == null
					&& board.pieceAt(bCol) == null
					&& !oppositeColorAttacks.contains(dCol.toString())
					&& !oppositeColorAttacks.contains(cCol.toString())) {
				Piece p = board.pieceAt(position.col - 4, position.row);
				if(p instanceof Rook && p.color == this.color) {
					Rook r = (Rook) p;
					if(r.numberOfMoves == 0) {
						this.availableMoves.add(cCol.toString());
					}
				}
			}
		}
	}

	@Override
	public String toString() {
		if(color == WHITE) {
			return "wK";
		}else {
			return "bK";
		}
	}

}
