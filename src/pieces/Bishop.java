package pieces;

import java.util.HashSet;

import chess.Board;
import chess.Square;
import pieceInterfaces.PieceMove;
import pieceInterfaces.SlidingPieceMove;

/**
 * A Bishop implementation of the abstract Piece class.
 * @author Abdulrahman Abdulrahman and Muhmmad Choudhary
 *
 */
public class Bishop extends Piece {

	/**
	 * Constructor for the bishop. Calls the super constructor.
	 * @param color - the piece's color
	 * @param coord - the String coordinate of the piece
	 * @param b - a reference to the board
	 */
	public Bishop(Color color, String coord, Board b) {
		super(color, coord, b);
	}
	
	@Override
	protected void fillMovesAndAttacks() {
		setBishopMoveSet(this);
	}

	/**
	 * A static method that adds a Bishop's move and attack set to the
	 * specified Piece. Used for both bishops and queens.
	 * @param p - the Piece to modify
	 */
	public static void setBishopMoveSet(Piece p) {
		
		PieceMove check = new SlidingPieceMove();
		
		// Upper left path
		HashSet<String> upperLeft = new HashSet<String>();
		int row = p.position.row - 1;
		int col = p.position.col - 1;
		while(row > -1 && col > -1) {
			Square s = new Square(col, row);
			int pathResult = check.moveCheck(p, s, upperLeft);
			if(pathResult == PieceMove.PATH_BLOCK) {
				break;
			}else if(pathResult == PieceMove.KING_ENCOUNTER) {
				try {
					King enemyKing = p.board.getOppositeColorKing(p.color);
					Square oneMore = new Square(col - 1, row - 1);
					if(enemyKing.availableMoves.contains(oneMore.toString())) {
						p.availableAttacks.add(oneMore.toString());
					}
				}catch(IllegalArgumentException e) {}
				break;
			}
			row--;
			col--;
		}
		
		// Bottom right path
		HashSet<String> bottomRight = new HashSet<String>();
		row = p.position.row + 1;
		col = p.position.col + 1;
		while(row < 8 && col < 8) {
			Square s = new Square(col, row);
			int pathResult = check.moveCheck(p, s, bottomRight);
			if(pathResult == PieceMove.PATH_BLOCK) {
				break;
			}else if(pathResult == PieceMove.KING_ENCOUNTER) {
				try {
					King enemyKing = p.board.getOppositeColorKing(p.color);
					Square oneMore = new Square(col + 1, row + 1);
					if(enemyKing.availableMoves.contains(oneMore.toString())) {
						p.availableAttacks.add(oneMore.toString());
					}
				}catch(IllegalArgumentException e) {}
				break;
			}
			row++;
			col++;
		}
		
		// Upper right path
		HashSet<String> upperRight = new HashSet<String>();
		row = p.position.row - 1;
		col = p.position.col + 1;
		while(row > -1 && col < 8) {
			Square s = new Square(col, row);
			int pathResult = check.moveCheck(p, s, upperRight);
			if(pathResult == PieceMove.PATH_BLOCK) {
				break;
			}else if(pathResult == PieceMove.KING_ENCOUNTER) {
				try {
					King enemyKing = p.board.getOppositeColorKing(p.color);
					Square oneMore = new Square(col + 1, row - 1);
					if(enemyKing.availableMoves.contains(oneMore.toString())) {
						p.availableAttacks.add(oneMore.toString());
					}
				}catch(IllegalArgumentException e) {}
				break;
			}
			row--;
			col++;
		}
		
		// Bottom left path
		HashSet<String> bottomLeft = new HashSet<String>();
		row = p.position.row + 1;
		col = p.position.col - 1;
		while(row < 8 && col > -1) {
			Square s = new Square(col, row);
			int pathResult = check.moveCheck(p, s, bottomLeft);
			if(pathResult == PieceMove.PATH_BLOCK) {
				break;
			}else if(pathResult == PieceMove.KING_ENCOUNTER) {
				try {
					King enemyKing = p.board.getOppositeColorKing(p.color);
					Square oneMore = new Square(col - 1, row + 1);
					if(enemyKing.availableMoves.contains(oneMore.toString())) {
						p.availableAttacks.add(oneMore.toString());
					}
				}catch(IllegalArgumentException e) {}
				break;
			}
			row++;
			col--;
		}
	}
	
	@Override
	public String toString() {
		if(color == WHITE) {
			return "wB";
		}else {
			return "bB";
		}
	}

}
