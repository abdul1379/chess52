package pieces;

import java.util.HashSet;

import chess.Board;
import chess.Square;
import pieceInterfaces.PieceMove;
import pieceInterfaces.SlidingPieceMove;

/**
 * A Rook implementation of the abstract SpecialFirstMovePiece class.
 * @author Abdulrahman Abdulrahman and Muhmmad Choudhary
 *
 */
public class Rook extends SpecialFirstMovePiece {
	
	/**
	 * Constructor for the piece. Calls the super constructor.
	 * @param color - the piece's color
	 * @param coord - the String coordinate of the piece
	 * @param b - a reference to the board
	 */
	public Rook(Color color, String coord, Board b) {
		super(color, coord, b);
	}
	
	/**
	 * Calls {@link Piece#moveTo(String)} and increments
	 * {@link SpecialFirstMovePiece#numberOfMoves}.
	 */
	@Override
	public void moveTo(String s) throws IllegalArgumentException {
		super.moveTo(s);
		numberOfMoves++;
	}

	@Override
	protected void fillMovesAndAttacks() {
		setRookMoveSet(this);
	}

	/**
	 * A static method that adds a Rook's move and attack set to the specified
	 * Piece. Used for both rooks and queens.
	 * @param p - the Piece to modify
	 */
	public static void setRookMoveSet(Piece p) {
		
		PieceMove check = new SlidingPieceMove();
		
		// Top path
		HashSet<String> topPath = new HashSet<String>();
		for(int row = p.position.row - 1; row > -1; row--) {
			Square s = new Square(p.position.col, row);
			int pathResult = check.moveCheck(p, s, topPath);
			if(pathResult == PieceMove.PATH_BLOCK) {
				break;
			}else if(pathResult == PieceMove.KING_ENCOUNTER) {
				try {
					King enemyKing = p.board.getOppositeColorKing(p.color);
					Square oneMore = new Square(p.position.col, row - 1);
					if(enemyKing.availableMoves.contains(oneMore.toString())) {
						p.availableAttacks.add(oneMore.toString());
					}
				}catch(IllegalArgumentException e) {}
				break;
			}
		}
		
		// Bottom path
		HashSet<String> bottomPath = new HashSet<String>();
		for(int row = p.position.row + 1; row < 8; row++) {
			Square s = new Square(p.position.col, row);
			int pathResult = check.moveCheck(p, s, bottomPath);
			if(pathResult == PieceMove.PATH_BLOCK) {
				break;
			}else if(pathResult == PieceMove.KING_ENCOUNTER) {
				try {
					King enemyKing = p.board.getOppositeColorKing(p.color);
					Square oneMore = new Square(p.position.col, row + 1);
					if(enemyKing.availableMoves.contains(oneMore.toString())) {
						p.availableAttacks.add(oneMore.toString());
					}
				}catch(IllegalArgumentException e) {}
				break;
			}
		}
		
		// Left path
		HashSet<String> leftPath = new HashSet<String>();
		for(int col = p.position.col - 1; col > -1; col--) {
			Square s = new Square(col, p.position.row);
			int pathResult = check.moveCheck(p, s, leftPath);
			if(pathResult == PieceMove.PATH_BLOCK) {
				break;
			}else if(pathResult == PieceMove.KING_ENCOUNTER) {
				try {
					King enemyKing = p.board.getOppositeColorKing(p.color);
					Square oneMore = new Square(col - 1, p.position.row);
					if(enemyKing.availableMoves.contains(oneMore.toString())) {
						p.availableAttacks.add(oneMore.toString());
					}
				}catch(IllegalArgumentException e) {}
				break;
			}
		}
		
		// Right path
		HashSet<String> rightPath = new HashSet<String>();
		for(int col = p.position.col + 1; col < 8; col++) {
			Square s = new Square(col, p.position.row);
			int pathResult = check.moveCheck(p, s, rightPath);
			if(pathResult == PieceMove.PATH_BLOCK) {
				break;
			}else if(pathResult == PieceMove.KING_ENCOUNTER) {
				try {
					King enemyKing = p.board.getOppositeColorKing(p.color);
					Square oneMore = new Square(col + 1, p.position.row);
					if(enemyKing.availableMoves.contains(oneMore.toString())) {
						p.availableAttacks.add(oneMore.toString());
					}
				}catch(IllegalArgumentException e) {}
				break;
			}
		}
	}
	
	@Override
	public String toString() {
		if(color == WHITE) {
			return "wR";
		}else {
			return "bR";
		}
	}

}
