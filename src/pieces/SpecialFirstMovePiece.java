package pieces;

import chess.Board;
import chess.Square;

/**
 * An abstract class for pieces that have special functions depending on whether
 * they have moved yet in the game.
 * @author Abdulrahman Abdulrahman and Muhmmad Choudhary
 *
 */
public abstract class SpecialFirstMovePiece extends Piece {

	/**
	 * Keeps count of the number of times this piece has moved.
	 */
	public int numberOfMoves = 0;
	/**
	 * Keeps track of this piece's starting Square.
	 */
	protected final Square startSquare;
	
	/**
	 * Constructor for the piece. Calls the super constructor.
	 * @param color - the piece's color
	 * @param coord - the String coordinate of the piece
	 * @param b - a reference to the board
	 */
	protected SpecialFirstMovePiece(Color color, String coord, Board b) {
		super(color, coord, b);
		startSquare = position;
	}

}
