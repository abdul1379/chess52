package pieces;

import chess.Board;

/**
 * A Queen implementation of the abstract Piece class.
 * @author Abdulrahman Abdulrahman and Muhmmad Choudhary
 *
 */
public class Queen extends Piece {

	/**
	 * Constructor for the piece. Calls the super constructor.
	 * @param color - the piece's color
	 * @param coord - the String coordinate of the piece
	 * @param b - a reference to the board
	 */
	public Queen(Color color, String coord, Board b) {
		super(color, coord, b);
	}

	@Override
	protected void fillMovesAndAttacks() {
		Bishop.setBishopMoveSet(this);
		Rook.setRookMoveSet(this);
	}

	@Override
	public String toString() {
		if(color == WHITE) {
			return "wQ";
		}else {
			return "bQ";
		}
	}

}
