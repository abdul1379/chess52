package pieceInterfaces;

import java.util.HashSet;

import chess.Square;
import pieces.King;
import pieces.Piece;


/**
 * An implementation of the PieceMove interface for sliding pieces.
 * @author Abdulrahman Abdulrahman and Muhmmad Choudhary
 */
public class SlidingPieceMove implements PieceMove {

	@Override
	public int moveCheck(Piece p, Square s, HashSet<String> path) {
		Piece temp = p.board.pieceAt(s);
		if(temp == null) {
			p.availableAttacks.add(s.toString());
			p.availableMoves.add(s.toString());
			path.add(s.toString());
			return PieceMove.PATH_CLEAR;
		}else if(temp.color == p.color) {
			p.availableAttacks.add(s.toString());
		}else {
			if(temp instanceof King) {
				King k = (King) temp;
				k.checkAttackPath = path;
				p.availableAttacks.add(s.toString());
				p.availableMoves.add(s.toString());
				return PieceMove.KING_ENCOUNTER;
			}
			p.availableAttacks.add(s.toString());
			p.availableMoves.add(s.toString());
		}
		return PieceMove.PATH_BLOCK;
	}


}
