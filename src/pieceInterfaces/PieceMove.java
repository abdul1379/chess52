package pieceInterfaces;

import java.util.HashSet;

import chess.Square;
import pieces.Piece;

/**
 * A functional interface for determining how a piece can move.
 * @author Abdulrahman Abdulrahman and Muhmmad Choudhary
 *
 */
public interface PieceMove {

	/**
	 * Represents a blocked path for the chess Piece.
	 */
	int PATH_BLOCK = -1;
	/**
	 * Represents a clear path for the chess Piece.
	 */
	int PATH_CLEAR = 0;
	/**
	 * Represents a path blocked by the enemy king for the chess Piece.
	 */
	int KING_ENCOUNTER = 1;
	
	/**
	 * Checks if a Piece can move to a specified Square.
	 * @param p - the Piece to check
	 * @param s - the Square to check
	 * @param path - the current attack path of the Piece. If an enemy king is
	 * encounted, the king's
	 * {@link pieces.King#checkAttackPath King.checkAttackPath} gets set to this
	 * path. Then, this path gets checked to see if any of the opposite color
	 * pieces can block the attack.
	 * @return {@link #PATH_CLEAR} if the Square is empty. {@link #PATH_BLOCK}
	 * if the Square is occupied by a piece. {@link #KING_ENCOUNTER} if the 
	 * Square is occupied by the enemy king.
	 */
	public int moveCheck(Piece p, Square s, HashSet<String> path);
}
