package chess;

/**
 * Represents a square on the chess board.
 * @author Abdulrahman Abdulrahman and Muhmmad Choudhary
 *
 */
public class Square {

	/**
	 * The column this square corresponds to.
	 */
	public int col;
	/**
	 * The row this square corresponds to.
	 */
	public int row;
	
	/**
	 * Constructor for a Square.
	 * @param col - the column this square corresponds to
	 * @param row - the row this square corresponds to
	 * @throws IllegalArgumentException if the square is out of bounds of the 
	 * chess board.
	 */
	public Square(int col, int row) throws IllegalArgumentException {
		if(col < 0 || row < 0 || col > 7 || row > 7) {
			throw new IllegalArgumentException();
		}
		this.col = col;
		this.row = row;
	}
	
	/**
	 * Static method to get a square corresponding to a String coordinate.
	 * @param s - the String coordinate to get the Square for
	 * @return A Square corresponding the the String coordinate.
	 * @throws IllegalArgumentException if the input String is out of bounds of
	 * the chess board.
	 */
	public static Square getSquare(String s) throws IllegalArgumentException {
		char[] c = s.toCharArray();
		return new Square(
				Character.getNumericValue(c[0]) - 10,
				8 - Character.getNumericValue(c[1]));
	}
	
	/**
	 * Static method to convert an input column and row to a String coordinate.
	 * @param col - the column of the square
	 * @param row - the row of the square
	 * @return A String corresponding to the input coordinate.
	 */
	public static String getCoordinate(int col, int row) {
		char[] c = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
		return "" + c[col] + (8 - row);
	}
	
	/**
	 * Returns the string coordinate representation of this Square.
	 */
	@Override
	public String toString() {
		return getCoordinate(this.col, this.row);
	}
	
	/**
	 * Used to compare Squares to other Squares, or compare Squares to String
	 * coordinates.
	 */
	@Override
	public boolean equals(Object o) {
		if(o instanceof Square) {
			Square s = (Square) o;
			return this.col == s.col && this.row == s.row;
		}else if(o instanceof String) {
			return this.toString().equals(o.toString());
		}
		return false;
	}
}
