package chess;

import java.util.Scanner;

import chess.Player.MoveResult;
import pieces.Piece.Color;

/**
 * The class containing the main method to start the program.
 * @author Abdulrahman Abdulrahman and Muhmmad Z. Choudhary
 *
 */
public class Chess {

	/**
	 * The main method to run the program.
	 * @param args - command line arguments (not used)
	 */
	public static void main(String[] args) {
		
		Board vals = new Board();
		Player white = vals.getPlayer(Color.WHITE);
		Player black = vals.getPlayer(Color.BLACK);
		
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		
		boolean whitesTurn = true;
		boolean drawProposal = false;
		
		System.out.println(vals);
		while(true) {
			if(whitesTurn) {
				System.out.print("White's move: ");
				String input = scan.nextLine();
				if(input.equals("resign")) {
					System.out.println("Black wins");
					return;
				} else if(drawProposal && input.equals("draw")) {
					System.out.println("draw");
					return;
				}
				String[] inputArray = input.split(" ");
				try {
					if(inputArray.length > 2
							&& inputArray[inputArray.length - 1].equals("draw?")) {
						drawProposal = true;
					}else {
						drawProposal = false;
					}
					MoveResult result = white.move(inputArray);
					if(result == MoveResult.CHECKMATE) {
						System.out.println();
						System.out.println(vals);
						System.out.println("Checkmate");
						System.out.println("White wins");
						return;
					}else if(result == MoveResult.CHECK) {
						System.out.println();
						System.out.println(vals);
						System.out.println("Check");
						whitesTurn = !whitesTurn;
						continue;
					}
				}catch(IllegalArgumentException e) {
					System.out.println("Illegal move, try again");
					continue;
				}
			}else {
				System.out.print("Black's move: ");
				String input = scan.nextLine();
				if(input.equals("resign")) {
					System.out.println("White wins");
					return;
				} else if(drawProposal && input.equals("draw")) {
					System.out.println("draw");
					return;
				}
				String[] inputArray = input.split(" ");
				try {
					if(inputArray.length > 2 &&
							inputArray[inputArray.length - 1].equals("draw?")) {
						drawProposal = true;
					}else {
						drawProposal = false;
					}
					MoveResult result = black.move(inputArray);
					if(result == MoveResult.CHECKMATE) {
						System.out.println();
						System.out.println(vals);
						System.out.println("Checkmate");
						System.out.println("Black wins");
						return;
					}else if(result == MoveResult.CHECK) {
						System.out.println();
						System.out.println(vals);
						System.out.println("Check");
						whitesTurn = !whitesTurn;
						continue;
					}
				}catch(IllegalArgumentException e) {
					System.out.println("Illegal move, try again");
					continue;
				}
			}
			System.out.println();
			System.out.println(vals);
			whitesTurn = !whitesTurn;
		}
	}

}
