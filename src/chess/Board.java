package chess;

import java.util.HashSet;

import pieces.King;
import pieces.Pawn;
import pieces.Piece;
import pieces.Piece.Color;
import pieces.SpecialFirstMovePiece;

/**
 * Represents the board that the game is played on.
 * @author Abdulrahman Abdulrahman and Muhmmad Choudhary
 *
 */
public class Board {
	
	/**
	 * A 2d array of pieces used to represent the game board.
	 */
	public Piece[][] board;
	
	/**
	 * A reference to the white player.
	 */
	public Player white;
	/**
	 * A reference to the black player.
	 */
	public Player black;
	
	/**
	 * A reference to any pawn that has just moved two spaces and can be
	 * attacked en passant.
	 */
	public Pawn enPassantPawn;
	/**
	 * A reference to any pawn that has reached the end of the board and needs
	 * to be promoted.
	 */
	public Pawn pawnToPromote;
	
	/**
	 * A reference to any piece that was just killed in case the board needs to
	 * be reverted and this piece needs to be put back in the game.
	 */
	public Piece tempPiece;
	
	/**
	 * An array that holds the last input from the player so any move can be
	 * undone.
	 */
	public String[] lastInput;
	
	/**
	 * No-args constructor for the board. When the board is initialized, the
	 * players are also initialized and they place their pieces on the board.
	 */
	public Board() {
		board =  new Piece[8][8];
		white = new Player(Color.WHITE, this);
		black = new Player(Color.BLACK, this);
	}
	
	/**
	 * Gets the piece at the specified square of the board.
	 * @param col - the column of the requested square
	 * @param row - the row of the requested square
	 * @return A Piece object if there was a piece at the square, or null if 
	 * the square is empty.
	 */
	public Piece pieceAt(int col, int row) {
		return board[row][col];
	}
	
	/**
	 * Gets the piece at the specified square of the board.
	 * @param s - the Square object that represents the requested square
	 * @return A Piece object if there was a piece at the square, or null if
	 * the square is empty.
	 */
	public Piece pieceAt(Square s) {
		return board[s.row][s.col];
	}
	
	/**
	 * Gets the piece at the specified coordinate of the board.
	 * @param s - the String coordinate representing the requested square
	 * @return A Piece object if there was a piece at the square, or null if
	 * the square is empty.
	 */
	public Piece pieceAt(String s) {
		return pieceAt(Square.getSquare(s));
	}
	
	/**
	 * Gets the requested Player object.
	 * @param c - the color of the requested player
	 * @return The white player, or black player depending on what was
	 * requested.
	 */
	public Player getPlayer(Color c) {
		if(c == Color.WHITE) {
			return white;
		}else {
			return black;
		}
	}
	
	/**
	 * Places a piece on the board at the Square specified by
	 * {@link Piece#position p.position}.
	 * @param p - the Piece to place on the board
	 * @throws IllegalArgumentException if there is already a piece on the
	 * board at the position specified by {@link Piece#position p.position}
	 */
	public void placePiece(Piece p) throws IllegalArgumentException {
		if(board[p.position.row][p.position.col] != null) {
			throw new IllegalArgumentException("Trying to double stack pieces");
		}else {
			board[p.position.row][p.position.col] = p;
		}
	}
	
	/**
	 * Removes the piece at the specified Square if there is one.
	 * Sets {@link #tempPiece tempPiece} equal to the specified square of the
	 * board.
	 * @param s - the Square object representing the coordinate to remove
	 */
	public void removePieceAt(Square s) {
		tempPiece = board[s.row][s.col];
		board[s.row][s.col] = null;
		if(tempPiece != null) {
			if(tempPiece.color == Color.WHITE) {
				white.pieces.remove(tempPiece);
			}else {
				black.pieces.remove(tempPiece);
			}
		}
	}
	
	/**
	 * Updates the available attacks and moves for both players.
	 * @param callerColor - the color of the Player calling this function. This
	 * is necessary because the order in which the players are updated can make
	 * a difference in special situations such as pawn promotion.
	 */
	public void updateAttacksAndMoves(Color callerColor) {
		if(callerColor == Color.WHITE) {
			white.fillMovesAndAttcks();
			black.fillMovesAndAttcks();
		}else {
			black.fillMovesAndAttcks();
			white.fillMovesAndAttcks();
		}
		
	}
	
	/**
	 * Checks if the opposite color player is in check.
	 * @param callerColor - the color of the Player calling this function.
	 * @return True if the opposite player is in check, and false otherwise.
	 */
	public boolean oppositeColorInCheck(Color callerColor) {
		if(callerColor == Color.WHITE) {
			return black.inCheck();
		}else {
			return white.inCheck();
		}
	}
	
	/**
	 * Gets the king of the opposite color. Necessary to decide if the last
	 * executed move resulted in Check or Checkmate.
	 * @param callerColor - the color of the Player calling this function.
	 * @return The King of the opposite color.
	 */
	public King getOppositeColorKing(Color callerColor) {
		if(callerColor == Color.WHITE) {
			return black.king;
		}else {
			return white.king;
		}
	}
	
	/**
	 * Gets all the available attacks of the opposite color player. Used for
	 * deciding the available moves for a King, and for deciding if the caller's
	 * King is under attack/in check.
	 * @param callerColor - the color of the Player/Piece calling this function.
	 * @return A HashSet of Strings that represent the coordinates that the
	 * other player can attack.
	 */
	public HashSet<String> getOppositeColorAttacks(Color callerColor){
		if(callerColor == Color.WHITE) {
			return black.availableAttacks;
		}else {
			return white.availableAttacks;
		}
	}
	
	/**
	 * Gets all the available moves of the opposite color player. Used for
	 * deciding if a piece can intersect an attack that would otherwise result
	 * in Checkmate.
	 * @param callerColor - the color of the Player calling this function.
	 * @return A HashSet of Strings that represent the coordinates that the
	 * other player's pieces can move to.
	 */
	public HashSet<String> getOppositeColorMoves(Color callerColor){
		if(callerColor == Color.WHITE) {
			return black.availableMoves;
		}else {
			return white.availableMoves;
		}
	}
	
	/**
	 * Undoes the last action on the board. This gets called when a player makes
	 * a move that results in their own King being put in Check.
	 */
	public void revert() {
		Square prev = Square.getSquare(lastInput[0]);
		Square curr = Square.getSquare(lastInput[1]);
		
		Piece moveBack = pieceAt(curr);
		moveBack.position = prev;
		board[prev.row][prev.col] = moveBack;
		
		if(moveBack instanceof SpecialFirstMovePiece) {
			((SpecialFirstMovePiece) moveBack).numberOfMoves--;
		}
		
		if(tempPiece != null) {
			board[curr.row][curr.col] = tempPiece;
			if(tempPiece.color == Color.WHITE) {
				white.pieces.add(tempPiece);
			}else {
				black.pieces.add(tempPiece);
			}
		}else {
			board[curr.row][curr.col] = null;
		}
		
		updateAttacksAndMoves(moveBack.color);
	}
	
	/**
	 * Returns the string representation of the board in the format specified by
	 * the assignment description.
	 */
	@Override
	public String toString() {
		StringBuilder string = new StringBuilder(250);
		
		for(int row = 0; row < 8; row ++) {
			for(int col = 0; col < 8; col++) {
				if(board[row][col] != null) {
					string.append(board[row][col]);
				}else {
					if((row + col) % 2 == 0) {
						string.append("  ");
					}else {
						string.append("##");
					}
				}
				string.append(" ");
			}
			string.append((8 - row) + "\n");
		}
		string.append(" a  b  c  d  e  f  g  h\n");
		
		return string.toString();
	}
}
