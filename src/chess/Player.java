package chess;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.function.Consumer;

import pieces.Bishop;
import pieces.King;
import pieces.Knight;
import pieces.Pawn;
import pieces.Piece;
import pieces.Queen;
import pieces.Rook;
import pieces.Piece.Color;

/**
 * Represents either the white player or the black player. All moves on the
 * board are done through the Player class.
 * @author Abdulrahman Abdulrahman and Muhmmad Choudhary
 *
 */
public class Player {
	
	/**
	 * An enum that holds the possible outcomes of a move.
	 * Used to determine if a move results in a Check or Checkmate.
	 * @author Abdulrahman Abdulrahman and Muhmmad Choudhary
	 *
	 */
	public enum MoveResult{
		SUCCESS, CHECK, CHECKMATE
	}

	/**
	 * The color of this player.
	 */
	public Color color;
	/**
	 * A reference to the chess board.
	 */
	public Board board;
	/**
	 * All the available moves that this player's pieces can move to.
	 */
	public HashSet<String> availableMoves = new HashSet<String>();
	/**
	 * All the squares that can be attacked by this player's pieces.
	 */
	public HashSet<String> availableAttacks = new HashSet<String>();
	/**
	 * A list that keeps the reference of this player's available pieces.
	 */
	public List<Piece> pieces = new ArrayList<Piece>(20);
	/**
	 * A reference to this player's king.
	 */
	public King king;
	
	/**
	 * Constructor for player. Initializes and places all of this player's
	 * pieces on the board.
	 * @param color - the player's color
	 * @param b - a reference to the board
	 */
	public Player(Color color, Board b) {
		this.color = color;
		this.board = b;
		if(color == Color.WHITE) {
			whiteSetup();
		}else {
			blackSetup();
		}
	}
	
	/**
	 * Used to initialize and place all of the white pieces on the board.
	 */
	private void whiteSetup() {
		// Pawns
		for(int i = 0; i < 8; i++) {
			pieces.add(new Pawn(color, Square.getCoordinate(i, 6), board));
		}
		
		// Rooks
		pieces.add(new Rook(color, "a1", board));
		pieces.add(new Rook(color, "h1", board));
		
		// Knights
		pieces.add(new Knight(color, "b1", board));
		pieces.add(new Knight(color, "g1", board));
		
		// Bishops
		pieces.add(new Bishop(color, "c1", board));
		pieces.add(new Bishop(color, "f1", board));
		
		// Queen
		pieces.add(new Queen(color, "d1", board));
		
		// King
		king = new King(color, "e1", board);
		pieces.add(king);
		
		// Putting all pieces on the board
		for(Piece p : pieces) {
			board.board[p.position.row][p.position.col] = p;
		}
	}
	
	/**
	 * Used to initialize and place all of the black pieces on the board.
	 */
	private void blackSetup() {
		// Pawns
		for(int i = 0; i < 8; i++) {
			pieces.add(new Pawn(color, Square.getCoordinate(i, 1), board));
		}
		
		// Rooks
		pieces.add(new Rook(color, "a8", board));
		pieces.add(new Rook(color, "h8", board));
		
		// Knights
		pieces.add(new Knight(color, "b8", board));
		pieces.add(new Knight(color, "g8", board));
		
		// Bishops
		pieces.add(new Bishop(color, "c8", board));
		pieces.add(new Bishop(color, "f8", board));
		
		// Queen
		pieces.add(new Queen(color, "d8", board));
		
		// King
		king = new King(color, "e8", board);
		pieces.add(king);
		
		// Putting all pieces on the board
		for(Piece p : pieces) {
			board.board[p.position.row][p.position.col] = p;
		}
	}
	
	/**
	 * Executes the move specified by args on the board.
	 * @param args - a String array which should be obtained by taking the
	 * player's input from the console and running {@link String#split(String)}
	 * on it.
	 * @return A {@link MoveResult} enum value that tells whether this move
	 * resulted in Check or Checkmate.
	 * @throws IllegalArgumentException if player tries to do an illegal move
	 */
	public MoveResult move(String[] args) throws IllegalArgumentException {
		board.lastInput = args;
		
		Piece p = board.pieceAt(args[0]);
		
		if(p == null || p.color != this.color) {
			throw new IllegalArgumentException("Invalid start square");
		}
		
		p.moveTo(args[1]);
		
		if(inCheck()) {
			board.revert();
			throw new IllegalArgumentException();
		}
		
		if(board.pawnToPromote != null) {
			promotion(args);
		}
		
		King oppositeColorKing = board.getOppositeColorKing(color);
		HashSet<String> oppositeColorAttacks = board.getOppositeColorAttacks(color);
		HashSet<String> oppositeColorMoves = board.getOppositeColorMoves(color);
		if(availableAttacks.contains(oppositeColorKing.position.toString())) {
			// Enemy king is in check
			if(oppositeColorKing.availableMoves.isEmpty()
					&& !oppositeColorAttacks.contains(p.position.toString())) {
				/*
				 * Gets here if the enemy king has no moves and if the enemy has
				 * no piece that can kill the one that was just moved.
				 */
				for(String s : oppositeColorKing.checkAttackPath) {
					if(oppositeColorMoves.contains(s)) {
						/*
						 * If there is a piece that can block the attack return
						 * check.
						 */
						return MoveResult.CHECK;
					}
				}
				
				// If no piece can block the attack, it's checkmate
				return MoveResult.CHECKMATE;
			}
			
			return MoveResult.CHECK;
		}
		
		return MoveResult.SUCCESS;
	}
	
	/**
	 * Used to promote a pawn if {@link Board#pawnToPromote} is not null.
	 * @param args - the same args that gets passed to
	 * {@link Player#move(String[])}. Used to determine which piece to promote
	 * to.
	 */
	private void promotion(String[] args) {
		Square promoteSquare = board.pawnToPromote.position;
		
		Consumer<Piece> promo = (p) -> {
			board.removePieceAt(promoteSquare);
			pieces.add(p);
			board.placePiece(p);
		};
		
		if(args.length > 2 && !args[2].equals("draw?")) {
			switch(args[2]) {
			case "R":
				promo.accept(
						new Rook(color, promoteSquare.toString(), board));
				break;
			case "N":
				promo.accept(
						new Knight(color, promoteSquare.toString(), board));
				break;
			case "B":
				promo.accept(
						new Bishop(color, promoteSquare.toString(), board));
				break;
			case "Q":
				promo.accept(
						new Queen(color, promoteSquare.toString(), board));
				break;
			default:
				board.revert();
				throw new IllegalArgumentException("Illegal promotion type");
			}
		}else {
			promo.accept(new Queen(color, promoteSquare.toString(), board));
		}
		board.updateAttacksAndMoves(color);
	}
	
	/**
	 * Checks if this player's king is currently under attack.
	 * @return True if the king is under attack and false otherwise.
	 */
	public boolean inCheck() {
		board.updateAttacksAndMoves(color);
		HashSet<String> underAttack = board.getOppositeColorAttacks(color);
		if(underAttack.contains(king.position.toString())) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * Iterates through all of this player's available pieces and add's each
	 * piece's available moves and attacks to this player's available moves and
	 * attacks.
	 */
	public void fillMovesAndAttcks() {
		availableMoves.clear();
		availableAttacks.clear();
		for(Piece p : pieces) {
			if(p instanceof King) {
				continue;
			}
			p.update();
			availableMoves.addAll(p.availableMoves);
			availableAttacks.addAll(p.availableAttacks);
		}
		king.update();
		availableMoves.addAll(king.availableMoves);
		availableAttacks.addAll(king.availableAttacks);
	}
}
